/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/*  Questão de prova de Joabe "3 <= N <= 15, N ímpar, para imprimir na tela uma
    'matriz' com asteriscos na primeira e ultima linhas, e na coluna central.
    A matriz é (N+2) por (N+2).
*/

#include <stdio.h>
#include <stdbool.h>
void main()
{
    int N = 0;
    //printf("\nEntre com numero N para as 'dimensoes' da matriz: ");
    //scanf("%d", &N);

    do
    {
        printf("\nEntre com numero N impar (entre 3 e 15) para as 'dimensoes' da matriz: ");
        scanf("%d", &N);
    } while ((N%2 == 0) || N<3 || N>15);

    for (int i = 0; i < (N+2); i++)
    {
        printf("\n");
        for (int j = 0; j < (N+2); j++)
        {
            if((i == 0) ||  (j == ((N/2)+1) ) || (i==(N+2)-1) )
            {
                printf(" %c ", '*');
            }
            else
            {
                printf("   ");
            }
        }
    }
    fflush(stdin);
    /*
        !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    printf("\n\nPressione qualquer tecla para encerrar o programa.");
    getchar();
    return;
}
