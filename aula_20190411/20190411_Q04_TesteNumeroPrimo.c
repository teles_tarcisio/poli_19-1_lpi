/*********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 11/04/2019: Q04
    Receber um número do usuário e verificar se é primo.
*/
#include <stdio.h>

int numero = 0;
int divisores = 0;

void main()
{
    int numero = 0;
    printf("\n\n >>>>> TESTE DE NUMEROS PRIMOS <<<<<");

    /*
        do-while para forcar usuario a fornecer numeros validos (inteiro, >= 1)
    */
    do {
        printf("\n\nEntre com um numero inteiro e positivo: ");
        scanf("%d", &numero);
        } while (numero <= 0);

    for (int i = 1; i <= numero; i++)   // variavel incremental do loop vai de 1 até o numero fornecido
    {
        /*
            se o numero fornecido for divisível por mais de um
            que o antecede, entao o numero nao é primo
        */
        if (numero % i == 0)
        {
            //printf("\ndividindo por: %d", i);
            divisores++;
        }
    }

    /*
        como o loop começa com i == 1, se (o contador)
        divisores > 2 então o numero não é primo.
    */
   if (divisores == 2)  // so precisa testar == 2 pq o primeiro a ser testado é 1
   {
       printf("\nO numero %d e' primo!", numero);
   }
   else
   {
       printf("\nO numero %d nao e' primo!", numero);
   }
   fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
   printf("\n\nPressione qualquer tecla para encerrar o programa. ");
   getchar();
   return;
}
