/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 11/04/2019: Q03
    Criar um algoritmo que entre com dez notas de cada aluno de uma turma de 20
    alunos e imprima: a) a média de cada aluno; b) a média da turma ;
    c) o percentual de alunos que tiveram médias maiores ou iguais a 5.0 
*/

# include <stdio.h>
# include <stdlib.h>
//# include <locale.h>    // lib para testar acentuação
/*
    como é somente uma turma de alunos, as notas de cada aluno serão
    aramazenadas numa matriz bidimensional:
    1o índice (linhas) -> aluno; 2o índice (colunas) -> notas;
*/
int const qtde_alunos = 5;
int const qtde_notas = 3;    // qtde de alunos e de notas variável, facilita testes

/*
    as dimensões da matriz a seguir podem ser mudadas antes da compilação,
    de forma a facilitar os testes (caso estas dimensões precisassem ser mudadas
    em tempo de execução, seria necessário declarar a matriz como variável LOCAL
    ao menos dentro da função MAIN)
*/


void main()
{   
    //setlocale(LC_ALL, "Portuguese");    // testando texto na linha de comando com acentos e etc
    
    float alunos_notas[qtde_alunos][qtde_notas];
    //  o laço a seguir armazena a nota de cada aluno:
    printf("\n >>>>> CALCULO DAS MEDIAS DOS ALUNOS E DA TURMA <<<<< ");
    for (int i = 0; i < qtde_alunos; i++)       
    {
        printf("\n >> Informe as notas do(a) %do(a) aluno(a) <<", (i+1));
        for (int j = 0; j < qtde_notas; j++)
        {
            printf("\nEntre com a %da nota do(a) %do(a) aluno(a): ", (j+1), (i+1));
            scanf("%f", &alunos_notas[i][j]);
            fflush(stdin);
            /*
             !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
            isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
            encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
            */
        }
    }
    
    //  calculando a média de cada aluno:
    float media_aluno = 0;
    float perc_vagabundos = 0;
    for (int i = 0; i < qtde_alunos; i++)
    {
        printf("\nMedia do(a) %do(a) aluno(a): ", (i+1));
        for (int j = 0; j < qtde_notas; j++)
        {
            media_aluno += alunos_notas[i][j]; 
        }
        media_aluno = (media_aluno/qtde_notas);
        if(media_aluno >= 5.00)
        {
            perc_vagabundos += 1;
        }
        printf("%.2f", media_aluno);
        media_aluno = 0; //zerar variavel para calculo de outro aluno
    }

    //  calculando a média da turma:
    printf("\nMedia geral da turma: ");
    float media_turma = 0;
    for (int i = 0; i < qtde_alunos; i++)    // calculo da media da turma
    {
        for (int j = 0; j < qtde_notas; j++)
        {
            media_turma += alunos_notas[i][j]; 
        }        
    }
    printf("%.2f", media_turma/(qtde_alunos*qtde_notas));
    
    //  calculando percentual de alunos com media >= 5 :
    printf("\n>> %.2f %% << dos alunos da turma obtiveram media maior ou igual a 5.00", (perc_vagabundos / qtde_alunos)*100);
    
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}