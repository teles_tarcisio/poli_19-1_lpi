/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 11/04/2019: Q02
    Num campeonato europeu de volleyball, se inscreveram 30 países.
    Sabendo-se que na lista oficial de cada país consta, além de outros dados,
    peso e idade de 12 jogadores, criar um algoritmo que apresente as seguintes informações: 
    a. o peso médio e idade média de cada um dos times; 
    b. o peso médio e a idade média de todos os participantes. 
*/

#include <stdio.h>
int quantidade = 1; //quantidade de paises variavel pra poder testar mais facilmente
float media_peso = 0;
float media_idade = 0;

void main()
{
    /*
        os vetores a seguir foram declarados como variáveis locais pois
        com a quantidade de paises podendo ser mudada, o compilador nao
        aceitava os vetores como variaveis globais
    */
    int paises[quantidade]; 
    int idades[quantidade][12];
    float pesos[quantidade][12];
    
    /*
        o laço a seguir armazena idade e peso de cada jogador, num país do campeonato
        por vez. Os dados nos vetores possuem o mesmo índice para os jogadores do mesmo país!
    */
    for (int i = 0; i < quantidade; i++)       
    {
        printf("\n >> Pais de numero %d <<", (i+1));

        for (int j = 0; j < 12; j++)
        {
            printf("\nEntre com peso do jogador %d do pais %d: ", (j+1), (i+1));
            scanf("%f", &pesos[i][j]);
            fflush(stdin);
            printf("\nEntre com idade do jogador %d do pais %d: ", (j+1), (i+1));
            scanf("%d", &idades[i][j]);
            fflush(stdin);
            /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
            isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
            encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
            */
        }
    }
    
    for (int i = 0; i < quantidade; i++)    // calculo das medias de peso e idade por país
    {
        printf("\nMedia de peso do %do pais: ", (i+1));
        for (int j = 0; j < 12; j++)
        {
            media_peso += pesos[i][j]; 
        }
        media_peso = (media_peso/12);
        printf("%.2f kg", media_peso);
        media_peso = 0; //zerar variavel para calculo de outro time

        printf("\nMedia de idade do %do pais: ", (i+1));
        for (int j = 0; j < 12; j++)
        {
            media_idade = /*(float)*/ media_idade + idades[i][j]; //testando typecasting
        }
        //media_idade = (media_idade/12);
        printf("%.1f anos", (media_idade/12)); //type casting implicito ao imprimir media_idade usando %f
        media_idade = 0; //zerar variavel para calculo de outro time
    }

    printf("\nMedia de peso de todos os jogadores: ");
    for (int i = 0; i < quantidade; i++)    // calculo da media de peso de todos os jogadores
    {
        for (int j = 0; j < 12; j++)
        {
            media_peso += pesos[i][j]; 
        }        
    }
    printf("%.2f kg", media_peso/(12*quantidade));
    
    printf("\nMedia de idade de todos os jogadores: "); // calculo da media de idade de todos os jogadores
    for (int i = 0; i < quantidade; i++)
    {
        for (int j = 0; j < 12; j++)
        {
            media_idade = /*(float)*/ media_idade + idades[i][j]; //testando typecasting
        }
    }
    printf("%.1f anos", media_idade/(12*quantidade)); //type casting implicito ao imprimir media_idade usando %f

    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}