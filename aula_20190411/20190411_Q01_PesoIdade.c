/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 11/04/2019: Q01
    Crie um algoritmo que receba a idade e o peso de 20 pessoas.
    Calcular e imprimir as médias dos pesos das pessoas da mesma faixa etária.
    As faixas etárias são: de 1 a 10 anos; de 11 a 20 anos; de 21 a 30 anos e maiores de 30 anos. 
*/

#include <stdio.h>

//int const quantidade = 20;

void main()
{
    int quantidade = 0;
    printf("\nDigite a quantidade de pessoas para o estudo: ");
    scanf("%d", &quantidade);
    fflush(stdin);

    int idades[quantidade];
    float pesos[quantidade];
    //float media01_10 = 0, media11_20 = 0, media21_30 = 0, media31_ = 0;
    float sum_pesos01_10 = 0, sum_pesos11_20 = 0, sum_pesos21_30 = 0, sum_pesos31_ = 0;
    int qtde01_10 = 0, qtde11_20 = 0, qtde21_30 = 0, qtde31_ = 0;
    /*
        o laço a seguir armazena idade e peso de cada pessoa numa posição para
        cada vetor respectivo a cada dado.
        Os dados nos vetores possuem o mesmo índice para a mesma pessoa!
    */
    for (int i = 0; i < quantidade; i++)       
    {
        printf("\nEntre com a idade da %da pessoa: ", (i+1));
        scanf("%d", &idades[i]);
        fflush(stdin);

        printf("\nEntre com o peso da %da pessoa: ", (i+1));
        scanf("%f", &pesos[i]);
        fflush(stdin);
    }
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    
    for (int i = 0; i < quantidade; i++)
    {
        if ( (idades[i] >=1) && (idades[i]<=10) )
        {
            qtde01_10 += 1;
            sum_pesos01_10 = sum_pesos01_10 + pesos[i];
        }
        else if ( (idades[i] >= 11) && (idades[i] <= 20) )
        {
            qtde11_20 += 1;
            sum_pesos11_20 = sum_pesos11_20 + pesos[i];   
        }
        else if( (idades[i] >= 21) && (idades[i] <= 30) )
        {
            qtde21_30 += 1;
            sum_pesos21_30 = sum_pesos21_30 + pesos[i];
        }
        else //if idades[i] > 30
        {
            qtde31_ += 1;
            sum_pesos31_ = sum_pesos31_ + pesos[i];
        }
    }
    printf("\n\n>>Total de pessoas: %d", quantidade);
    printf("\n\n\tPessoas com 01 a 10 anos: %d", qtde01_10);
    printf("\n\tSoma pesos 01 a 10 anos: %.2f", sum_pesos01_10);
    printf("\n\tMedia dos pesos 01 a 10 anos: %.2f", sum_pesos01_10 / qtde01_10);
    
    printf("\n\n\t\tPessoas com 11 a 20 anos: %d", qtde11_20);
    printf("\n\t\tSoma pesos 11 a 20 anos: %.2f", sum_pesos11_20);
    printf("\n\t\tMedia dos pesos 11 a 20 anos: %.2f", sum_pesos11_20 / qtde11_20);

    printf("\n\n\t\t\tPessoas com 21 a 30 anos: %d", qtde21_30);
    printf("\n\t\t\tSoma pesos 21 a 30 anos: %.2f", sum_pesos21_30);
    printf("\n\t\t\tMedia dos pesos 21 a 30 anos: %.2f", sum_pesos21_30 / qtde21_30);

    printf("\n\n\t\t\t\tPessoas com 31 anos ou mais: %d", qtde31_);
    printf("\n\t\t\t\tSoma pesos 31+ anos: %.2f", sum_pesos31_);
    printf("\n\t\t\t\tMedia dos pesos 31+ anos: %.2f", sum_pesos31_ / qtde31_);
    
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}