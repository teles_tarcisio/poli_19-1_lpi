/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 11/04/2019: Q05
    Uma pousada estipulou o preço para a sua diária em R$30,00 e mais uma taxa de
    serviços diários de:
    a) R$15,00 se o número de dias for menor que 10;
    b) R$8,00 se o número de dias for maior ou igual a 10;
    Criar um algoritmo que imprima nome, conta e número da conta de cada cliente e
    ao final o total ganho pela pousada.
*/

# include <stdio.h>
//# include <stdlib.h>
//# include <locale.h>    // lib para testar acentuação
#define total_quartos 8
//const int total_quartos = 8;    // quantidade de quartos da pousada (ocupacao maxima)

void main()
{
    //setlocale(LC_ALL, "Portuguese");    // testando texto na linha de comando com acentos e etc
    /*
        a matriz "clientes" guarda nas colunas os nomes dos clientes (até 30 caracteres),
        o índice das linhas desta matriz é o "número do quarto - 1" onde o cliente está.
        Ex.: cliente[0] == "Astolfo Meu Cachorro" significa
        "Astolfo Meu Cachorro está hospedado no quarto 1"

        o vetor "contas" guarda o valor da conta de cada quarto/hóspede,
        o índice neste vetor é o "número do quarto - 1" assim como anteriormente

        o vetor "diarias" guarda a quantidade de diárias de cada quarto/hóspede,
        o índice neste vetor é o "número do quarto - 1" assim como anteriormente
    */
    int indice = 0;
    char clientes[total_quartos][30];
    float contas[total_quartos];
    int diarias[total_quartos];
    for (int i = 0; i < total_quartos; i++)
    {
        diarias[total_quartos] = -1;
    }   // valor -1 neste vetor significa quarto vago

    printf("\n >>>>> CONTROLE DAS DIARIAS DA POUSADA <<<<< ");
    char opcao_menu = '0';
    printf("\n Escolha a opcao desejada usando o menu: ");
    printf("\n\t[N]ovo hospede \t\t\t[L]istar hospedes/quartos");
    printf("\n\t[F]echar conta do quarto \t[I]mprimir faturamento na tela");
    printf("\n\t\t\t\t[Q] para encerrar o programa\n >> ");
    scanf("%c", &opcao_menu);
    fflush(stdin);

    switch (opcao_menu)
        {
            case 'N':
            case 'n':
                printf("\nInforme o nome do cliente: ");
                char aux[30] = "";
                gets(aux);
                if (diarias[indice] != -1)
                {
                    indice = indice;
                }
                else
                {
                    printf("\n%s ficara' no quarto %d.", aux, indice);
                    printf("\nInforme a quantidade de diarias nesta hospedagem: ");
                    scanf("%d", &diarias[indice]);
                    if (diarias[indice] < 10)
                    {
                        contas[indice] = diarias[indice]*30 + diarias[indice]*15;
                    }
                    else if (diarias[indice] >= 10)
                    {
                        contas[indice] = diarias[indice]*30 + diarias[indice]*8;
                    }
                    indice += 1;
                }
                //break;
            case 'L':
            case'l':
                printf("\nRelacao de hospedes e quartos: ");
                for (int i = 0; i < total_quartos; i++)
                {
                    printf("\n%s, quarto %d", clientes[i]);
                    printf(", valor total para %d diarias R$ %.2f", diarias[i], contas[i]);
                }
                printf("\n\nPressione qualquer tecla para encerrar o programa. ");
                getchar();
                //return;
                break;
            case 'F':
            case 'f':
                printf("\nSelecione o quarto para encerrar a conta: ");
                int aux2 = 0;
                scanf("%d", &aux2);
                printf("Quarto de numero %d, %d diarias, valor total R$%.2f", aux2, diarias[aux2], contas[aux2]);
                printf("\n\nPressione qualquer tecla para encerrar o programa. ");
                fflush(stdin);
                getchar();
                //return;
                break;
            default:
                printf("\nSelecione uma opcao valida! O programa sera encerrado");
                fflush(stdin);
                getchar();
                return; //encerra o programa
                //break;
        }

    //} while((opcao_menu != 'q') || (opcao_menu != 'Q'));
    // switch para o menu

    fflush(stdin);
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
