/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 16/04/2019: Q01
    Faça um algoritmo que calcule a soma dos N primeiros números primos,
    sendo N definido pelo usuário no módulo principal.
    O algoritmo ainda deverá ter os módulos Soma_Primos e Primo,
    sendo que o primeiro será responsável pela soma dos números que forem primos e
    o segundo será responsável por verificar se o número em questão é primo ou não.
*/

#include <stdio.h>
#include "Primo.h"
#include "Primo.c"
#include "SomarPrimos.h"
#include "SomarPrimos.c"
//#include "./lib/SomarPrimos.c"
//#include "./include/Primo.h"
//#include "./include/SomarPrimos.h"

int num = 0;

void main()
{
    printf("\nEntre com o numero a ser conferido: ");
    scanf("%d", &num);
    fflush(stdin);
    /*
    !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
    isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
    encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    printf("resultado primo: %d", Primo(num));
    printf("resultado somaprimo: %d", SomarPrimos(num));
    /*
    if( Primo(num) == 1 )
    {
        printf("\nretorno de Primo 1, numero e' primo");
    }
    else if( Primo(num) == 0 )
    {
        printf("\nretorno de Primo 0, numero nao e' primo");
    }
    */

    /*
    TODO: assegurar que o N fornecido pelo usuario é unsigned int
    ( 0 <= N <= 65535), aqui no programa principal!!
    */

    fflush(stdin);
    /*
    !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
    isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
    encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
