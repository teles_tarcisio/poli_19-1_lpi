/*********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 11/04/2019: Q04
    Receber um número do usuário e verificar se é primo.
*/
#include <stdio.h>
#include <stdbool.h>
bool  Primo(int numero)
{
    int divisores = 0;
    // variavel incremental do loop vai de 1 até o numero fornecido
    for (int i = 1; i <= numero; i++)
    {
        /*
            se o numero fornecido for divisível por mais de um
            que o antecede, entao o numero nao é primo
        */
        if (numero % i == 0)
        {
            divisores++;
        }
    }
    /*
        como o loop começa com i == 1, se (o contador)
        divisores => 2 ANTES de i se igualar a numero,
        então o numero não é primo
    */
   if (divisores == 2)  // so precisa testar == 2 pq o primeiro a ser testado é 1
   {
       // printf("\nO numero %d e' primo!", numero);
       return 1;
   }
   else
   {
       // printf("\nO numero %d nao e' primo!", numero);
       return 0;
   }
}
