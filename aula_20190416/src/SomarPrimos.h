/*********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 11/04/2019: Q04
    Dado um número N pelo usuário, calcular a soma destes N primeiros
    números primos. Ex.: N = 3 -> somar 3 primeiros primos = 2 + 3 + 5 = 17
*/
#include <stdbool.h>
unsigned int SomarPrimos(const unsigned int N);
