/*********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 11/04/2019: Q04
    Dado um número N pelo usuário, calcular a soma destes N primeiros
    números primos. Ex.: N = 3 -> somar 3 primeiros primos = 2 + 3 + 5 = 17
*/
#include <stdio.h>
#include <stdbool.h>
//#include "../include/Primo.h"
#include "Primo.h"
//#include "Primo.c"

//  valor maximo que N pode assumir:
const unsigned int MAXIMO = 65535;
bool vetor_primos[65535];

unsigned int SomarPrimos(const unsigned int N)
{
    int result_soma = 0;
    if (Primo(N) == 0)
    {
        return 0;
    }
    else
    {
        // printf("\nPreenchendo vetor_primos: \n");
        for (int k = 0; k <= N; k++)
        {
            vetor_primos[k] = 0;
            printf("vetor_primos[%d] = %d\n", k, vetor_primos[k] );
            printf("\nTestando se o indice do vetor e' primo ou nao: \n");
            for (int j = 0; j <= N; j++)
            {
                vetor_primos[j] = Primo(j);
                printf("vetor_primos[%d] = %d\n", j, vetor_primos[j]);
            }
            for (int t = 2; t <= N; t++)
            {
                // LEMBRETE op ternário -->  condição ? expressão_1 : expressão_2;
                if (vetor_primos[t] == 0)
                {
                    result_soma = result_soma;
                }
                else if (vetor_primos[t] == 1)
                {
                    result_soma = result_soma + t;
                }
            }
        }
    }
/*
--> retornar ??? se o N informado for maior que 65535;
--> retornar a soma se N for válido;
*/
return result_soma;
}
