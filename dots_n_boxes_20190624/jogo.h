#ifndef JOGO_H_INCLUDED
#define JOGO_H_INCLUDED

typedef struct jogador // estrutura para salvar informacoes do jogador.
{
    char nick[16];
    int vitoria;
    int boxes;
} player;

int tab(int boxes1, int boxes2, player player1, player player2);
void gamer();
void menu();
void ranking(int boxes, int boxes2, player player1, player player2);

#endif // JOGO_H_INCLUDED
