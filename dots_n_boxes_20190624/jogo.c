#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <windows.h>
#include <conio.h>
#include "GUI.h"
#include "jogo.h"

//------------------------------------------------- TABULEIRO -------------------------------------
int tab(int boxes1, int boxes2, player player1, player player2)
{
    //=============================================== ESCRITA DO TABULEIRO ========================
    system("color 0C");

    int pp1 = 0, pp2 = 0;
    /* variaveis de controle de jogadas do jogador 1 e 2, comecam com mesmo valor, que indica
        vez do jogador 1, sao incrementadas apos cada jogada feita pelo jogador
    */
    int i, j, g = 0;
    /*
        variavel "g" controla numero total de jogadas [(i, j) imprime matriz]
    */
    //TODO: modificar "formato" de tab[i][j] !!!
    // informacoes que ficam dentro da Matriz
    char tab[7][7] = {'*','a','*','b','*','c','*','d',' ','e',' ','f',' ','g','*','h','*','i','*','j','*','k',' ','l',' ','m',' ','n','*','o','*','p','*','q','*','r',' ','s',' ','t',' ','u','*','v','*','w','*','x','*'};

    char select; // variavel de escolha/selecao, captura a jogada e muda no tabuleiro
    while(g < 64)
    /*
    por enquanto, 64 movimentos, sao apenas 24 movimentos possiveis considerando
    mais um (25 no total ou um emenda pra parar o while) pra mostrar o vencedor
    */
    {
        cabecalho();
        if( boxes1 == 9 || boxes2 == 9 || (boxes1 + boxes2) == 9 )
            g = 100;
            for(i = 0; i < 7; i++)
            {
                printf("\t\t\t");
                for(j = 0; j < 7; j++)
                {
                    printf(" %c", tab[i][j]);
                }
                printf("\n");
            }
        printf("\n\nDigite \"z\" caso queira voltar para o Menu\n");
        printf("\nDigite letra na posicao da jogada\n\n\n\n\n\n\n ");
        select = getch();

// ========================================== SWITCH PARA AS ESCOLHAS =============================
        if(pp1 == pp2) // variaveis iguais, sempre que jogador1 efetua jogada
        {
            system("color 0C");
            switch(select)
            {
            case 'a':
                tab[0][1]='-';
                g++;
                pp1++;
                break;
            case 'b':
                tab[0][3]='-';
                g++;
                pp1++;
                break;
            case 'c':
                tab[0][5]='-';
                g++;
                pp1++;
                break;
            case 'd':
                tab[1][0]='|';
                g++;
                pp1++;
                break;
            case 'e':
                tab[1][2]='|';
                g++;
                pp1++;
                break;
            case 'f':
                tab[1][4]='|';
                g++;
                pp1++;
                break;
            case 'g':
                tab[1][6]='|';
                g++;
                pp1++;
                break;
            case 'h':
                tab[2][1]='-';
                g++;
                pp1++;
                break;
            case 'i':
                tab[2][3]='-';
                g++;
                pp1++;
                break;
            case 'j':
                tab[2][5]='-';
                g++;
                pp1++;
                break;
            case 'k':
                tab[3][0]='|';
                g++;
                pp1++;
                break;
            case 'l':
                tab[3][2]='|';
                g++;
                pp1++;
                break;
            case 'm':
                tab[3][4]='|';
                g++;
                pp1++;
                break;
            case 'n':
                tab[3][6]='|';
                g++;
                pp1++;
                break;
            case 'o':
                tab[4][1]='-';
                g++;
                pp1++;
                break;
            case 'p':
                tab[4][3]='-';
                g++;
                pp1++;
                break;
            case 'q':
                tab[4][5]='-';
                g++;
                pp1++;
                break;
            case 'r':
                tab[5][0]='|';
                g++;
                pp1++;
                break;
            case 's':
                tab[5][2]='|';
                g++;
                pp1++;
                break;
            case 't':
                tab[5][4]='|';
                g++;
                pp1++;
                break;
            case 'u':
                tab[5][6]='|';
                g++;
                pp1++;
                break;
            case 'v':
                tab[6][1]='-';
                g++;
                pp1++;
                break;
            case 'w':
                tab[6][3]='-';
                g++;
                pp1++;
                break;
            case 'x':
                tab[6][5]='-';
                g++;
                pp1++;
                break;
            case 'z': // tecla designada para finalizar jogo e chamar MENU
                system("cls");
                menu();
                break;
            default :
                printf("Jogada invalida ou ja efetuada!!!");
                Sleep(1500);
                break;
            }
            system("cls");

//========================================== BOXES1 ===============================================
            if( tab[0][1]=='-' && tab[1][0]=='|' && tab[1][2]=='|' && tab[2][1]=='-')
            {
                if( tab[1][1]== ' ' )
                {
                    tab[1][1]= '1';
                    boxes1++;    // variavel controle de pontuacao, incremento significa que o jogador1 pontuou
                    pp1--; // pp1 eh decrementada caso jogador feche uma caixa, para o mesmo jogador jogar novamente
                }
            }
            if( tab[0][3]=='-' && tab[1][2]=='|' && tab[1][4]=='|' && tab[2][3]=='-')
            {
                if( tab[1][3]== ' ' )
                {
                    tab[1][3]= '1';
                    boxes1++;
                    pp1--;
                }
            }
            if( tab[0][5]=='-' && tab[1][4]=='|' && tab[1][6]=='|' && tab[2][5]=='-')
            {
                if( tab[1][5]== ' ')
                {
                    tab[1][5]= '1';
                    boxes1++;
                    pp1--;
                }
            }
            if( tab[2][1]=='-' && tab[3][0]=='|' && tab[3][2]=='|' && tab[4][1]=='-')
            {
                if( tab[3][1]== ' ')
                {
                    tab[3][1]= '1';
                    boxes1++;
                    pp1--;
                }
            }
            if( tab[2][3]=='-' && tab[3][2]=='|' && tab[3][4]=='|' && tab[4][3]=='-')
            {
                if( tab[3][3]== ' ')
                {
                    tab[3][3]= '1';
                    boxes1++;
                    pp1--;
                }
            }
            if( tab[2][5]=='-' && tab[3][4]=='|' && tab[3][6]=='|' && tab[4][5]=='-')
            {
                if( tab[3][5]== ' ')
                {
                    tab[3][5]= '1';
                    boxes1++;
                    pp1--;
                }
            }
            if( tab[4][1]=='-' && tab[5][0]=='|' && tab[5][2]=='|' && tab[6][1]=='-')
            {
                if( tab[5][1]== ' ')
                {
                    tab[5][1]= '1';
                    boxes1++;
                    pp1--;
                }
            }
            if( tab[4][3]=='-' && tab[5][2]=='|' && tab[5][4]=='|' && tab[6][3]=='-')
            {
                if( tab[5][3]== ' ')
                {
                    tab[5][3]= '1';
                    boxes1++;
                    pp1--;
                }
            }
            if( tab[4][5]=='-' && tab[5][4]=='|' && tab[5][6]=='|' && tab[6][5]=='-')
            {
                if( tab[5][5]== ' ')
                {
                    tab[5][5]= '1';
                    boxes1++;
                    pp1--;
                }
            }
        } //closes if
        // ----------
        else if( pp1 != pp2 )
        {
            system("color 0F");
            switch(select)
            {
            case 'a':
                tab[0][1]='-';
                g++;
                pp2++;
                break;
            case 'b':
                tab[0][3]='-';
                g++;
                pp2++;
                break;
            case 'c':
                tab[0][5]='-';
                g++;
                pp2++;
                break;
            case 'd':
                tab[1][0]='|';
                g++;
                pp2++;
                break;
            case 'e':
                tab[1][2]='|';
                g++;
                pp2++;
                break;
            case 'f':
                tab[1][4]='|';
                g++;
                pp2++;
                break;
            case 'g':
                tab[1][6]='|';
                g++;
                pp2++;
                break;
            case 'h':
                tab[2][1]='-';
                g++;
                pp2++;
                break;
            case 'i':
                tab[2][3]='-';
                g++;
                pp2++;
                break;
            case 'j':
                tab[2][5]='-';
                g++;
                pp2++;
                break;
            case 'k':
                tab[3][0]='|';
                g++;
                pp2++;
                break;
            case 'l':
                tab[3][2]='|';
                g++;
                pp2++;
                break;
            case 'm':
                tab[3][4]='|';
                g++;
                pp2++;
                break;
            case 'n':
                tab[3][6]='|';
                g++;
                pp2++;
                break;
            case 'o':
                tab[4][1]='-';
                g++;
                pp2++;
                break;
            case 'p':
                tab[4][3]='-';
                g++;
                pp2++;
                break;
            case 'q':
                tab[4][5]='-';
                g++;
                pp2++;
                break;
            case 'r':
                tab[5][0]='|';
                g++;
                pp2++;
                break;
            case 's':
                tab[5][2]='|';
                g++;
                pp2++;
                break;
            case 't':
                tab[5][4]='|';
                g++;
                pp2++;
                break;
            case 'u':
                tab[5][6]='|';
                g++;
                pp2++;
                break;
            case 'v':
                tab[6][1]='-';
                g++;
                pp2++;
                break;
            case 'w':
                tab[6][3]='-';
                g++;
                pp2++;
                break;
            case 'x':
                tab[6][5]='-';
                g++;
                pp2++;
                break;
            case 'z':
                system("cls");
                menu();
                break; //TODO: este break é executado mesmo com o menu() antes?
            default :
                printf("Jogada invalida ou ja efetuada!!!");
                Sleep(1500);
            } //closes switch
            system("cls");
//========================================== BOXES2 ===============================================
            if( tab[0][1]=='-' && tab[1][0]=='|' && tab[1][2]=='|' && tab[2][1]=='-')
            {
                if( tab[1][1]== ' ')
                {
                    tab[1][1]= '2';
                    boxes2++;
                    pp2--;
                }
            }
            if( tab[0][3]=='-' && tab[1][2]=='|' && tab[1][4]=='|' && tab[2][3]=='-')
            {
                if( tab[1][3]== ' ')
                {
                    tab[1][3]= '2';
                    boxes2++;
                    pp2--;
                }
            }
            if( tab[0][5]=='-' && tab[1][4]=='|' && tab[1][6]=='|' && tab[2][5]=='-')
            {
                if( tab[1][5]== ' ')
                {
                    tab[1][5]= '2';
                    boxes2++;
                    pp2--;
                }
            }
            if( tab[2][1]=='-' && tab[3][0]=='|' && tab[3][2]=='|' && tab[4][1]=='-')
            {
                if( tab[3][1]== ' ')
                {
                    tab[3][1]= '2';
                    boxes2++;
                    pp2--;
                }
            }
            if( tab[2][3]=='-' && tab[3][2]=='|' && tab[3][4]=='|' && tab[4][3]=='-')
            {
                if( tab[3][3]== ' ')
                {
                    tab[3][3]= '2';
                    boxes2++;
                    pp2--;
                }
            }
            if( tab[2][5]=='-' && tab[3][4]=='|' && tab[3][6]=='|' && tab[4][5]=='-')
            {
                if( tab[3][5]== ' ')
                {
                    tab[3][5]= '2';
                    boxes2++;
                    pp2--;
                }
            }
            if( tab[4][1]=='-' && tab[5][0]=='|' && tab[5][2]=='|' && tab[6][1]=='-')
            {
                if( tab[5][1]== ' ')
                {
                    tab[5][1]= '2';
                    boxes2++;
                    pp2--;
                }
            }
            if( tab[4][3]=='-' && tab[5][2]=='|' && tab[5][4]=='|' && tab[6][3]=='-')
            {
                if( tab[5][3]== ' ')
                {
                    tab[5][3]= '2';
                    boxes2++;
                    pp2--;
                }
            }
            if( tab[4][5]=='-' && tab[5][4]=='|' && tab[5][6]=='|' && tab[6][5]=='-')
            {
                if( tab[5][5]== ' ')
                {
                    tab[5][5]= '2';
                    boxes2++;
                    pp2--;
                }
            }
        }// closes else-if
    }// closes while
    printf(" %d %d\n\n\n", boxes1, boxes2);
    return boxes1, boxes2;
}// closes int tab()
//=================================================================================================
void gamer()
/*
    funcao solicita nome dos jogadores, e chama funcao TAB(tabuleiro).
*/
//TODO: falta retornar valores do resultado do tabuleiro para ranking E ENCERRAR FUNCAO ANTES DE CHAMAR TAB()
{
    cabecalho();
    int contagem1=0, contagem2=0;// pontuacao do jogadores 1 e 2 respectivamente.

    player player1; // variaveis para nome do do play 1
    player player2;

    printf("\nDigite o nome do jogador numero 1 \n");
    scanf("%s", &player1.nick);
    system("cls");

    cabecalho();
    printf("\nDigite o nome do jogador numero 2 \n");
    scanf("%s", &player2.nick);
    system("cls");

    cabecalho();
    printf("Prontos? %s e %s \n\n\n", player1.nick, player2.nick);
    system("pause");
    system("cls");
    tab(contagem1, contagem2, player1, player2);
    printf(" %d %d\n\n\n", contagem1, contagem2);
    return;
} //closes gamer()
//=================================================================================================
FILE *rnk;  //variavel ponteiro global para manipulacao do arquivo txt
char leitura[888];
void menu() // menu de jogo, chama as funcoes para iniciar, mostrar instrucoes, ranking e saida.
{
    int escolha;

    system("color 1F");
    do
    {
        cabecalho();
        printf("\n\n\n");
        printf("\t\t1. Jogar\n");
        printf("\t\t2. Instrucoes\n");
        printf("\t\t3. Ranking\n");
        printf("\t\t4. Sair do jogo");
        printf("\t\t\n\n\n Escolha o numero da opcao acima -> ");
        scanf("%d", &escolha);
        system("cls");
    }
    while(escolha < 1 || escolha > 4);
    switch(escolha)
    {
    case 1:
        gamer();
        break;
    case 2:
        instrucoes();
        break;
    case 3:
        rnk = fopen("ranking_dots_boxes.txt","r"); //TODO: falta declarar rnk aqui (ponteiro)
        if(rnk == NULL)
        {
            printf("Nao foi possivel abrir o arquivo (NULL)\n");
            system("pause");
        }
        while(fgets(leitura,888,rnk) != NULL)
        {
            printf("%s", leitura);
        }
        fclose(rnk);
        break;
    case 4:
        //exit(-1);
        return;
        break;
    }
}// closes menu()

//=================================================================================================
void ranking(int boxes1, int boxes2, player player1, player player2)
{
    char Operacao;
    rnk = fopen("Ranking dots and boxes.txt", "w");
    fprintf(rnk,"Ranking\n\nJogador 1 %s = %d boxes\nJogador 2 %s = %d boxes\n", player1.nick, boxes1, player2.nick, boxes2);
    fclose(rnk);

    if ( boxes1 > boxes2)
    {
        printf("O jogador 1 venceu\n");
    }
    else
    {
        printf("O jogador 2 venceu\n");
    }

    printf("O placar foi de %d a %d\nE ele foi salvo no ranking\n", boxes1, boxes2);
    printf("Gostaria de ver o ranking? <S/N>\n");
    Operacao = getche();
    printf("\n\n");
    if (Operacao == 'S' || Operacao == 's')
    {
        rnk = fopen("Ranking dots and boxes.txt", "r");
        if(rnk == NULL)
        {
            printf("Nao foi possivel abrir o arquivo (NULL)\n");
            system("pause");
        }
        while(fgets(leitura,888,rnk) != NULL)
        {
            printf("%s", leitura);
        }
        fclose(rnk);

    }
    else{
        system("pause");
        }
}// closes ranking


