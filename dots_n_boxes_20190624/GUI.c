#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <windows.h>
#include <conio.h>
#include "GUI.h"
#include "jogo.h"

//========================================= Tela Inicial do Jogo ==================================
void inicio() // animacao exibida no inicio do programa
{

    system("color 5D");
    printf("\n\n\n\n\n\n\n\t\tD");
    Sleep(250);
    printf("O");
    Sleep(250);
    printf("T");
    Sleep(250);
    printf("S");
    Sleep(250);
    printf(" ");
    Sleep(250);
    printf("'N");
    Sleep(250);
    printf(" ");
    Sleep(250);
    printf("B");
    Sleep(250);
    printf("O");
    Sleep(250);
    printf("X");
    Sleep(250);
    printf("E");
    Sleep(250);
    printf("S\n\n\n");
    Sleep(400);
    system("cls");
    printf("\n\n\n\n\n\n\n\t\tDOTS 'N BOXES\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    Sleep(600);
    system("cls");
    Sleep(600);

    printf("\n\n\n\n\n\n\n\t\tDOTS 'N BOXES\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    Sleep(600);
    system("cls");
    Sleep(600);

    printf("\n\n\n\n\n\n\n\t\tDOTS 'N BOXES\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    Sleep(600);
    system("cls");
    Sleep(600);

    printf("\n\n\n\n\n\n\n\t\tDOTS 'N BOXES\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    Sleep(2000);
    system("cls");
    return;
} // closes inicio()

//========================================= funcao cabeçalho ======================================
void cabecalho() //funcao cabecalho, exibida em todas as partes do jogo exceto na funcao "inicio".
{
    for(int i = 0; i < 60; i++)
    {
        printf("-");
    }
    printf("\n\t\t\t DOTS 'N BOXES\n");
    for(int i = 0; i < 60; i++)
    {
        printf("-");
    }
    printf("\n\n");
    return;
}// closes cab();

void instrucoes() // funcao que mostra as instrucoes de jogo
{
    system("color 7C");
    cabecalho();

    printf("\n\n\n");
    printf("\t\tApos digitar seus dados,\n\t a partida comeca mostrando o tabuleiro\n\t com pontos e letras,\n");
    printf("\t\tselecione uma letra minuscula\n\t e aperte enter para escolher a posicao\n\t que deseja jogar.\n\n");
    printf("\t\tQuem conseguir completar um\n\t quadrado antes, tera o numero 1(player one)\n\t\tou numero 2 (player two)\n");
    printf("\tdentro do box, vence que tiver mais boxes\n\n\n\n");
    system("PAUSE");
    system("cls");
    menu();
    //TODO: CORRIGIR ESTE RETORNO DA FUNCAO, TEM QUE ENCERRAR E VOLTAR À PRINCIPAL!!
}// closes instructions()
