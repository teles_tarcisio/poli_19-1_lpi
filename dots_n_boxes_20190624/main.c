/*-------------------------------------------------------------------------------------------------
    Universidade de Pernambuco
    Escola Politécnica de Pernambuco
    LINGUAGEM DE PROGRAMAÇÃO IMPERATIVA - 2019.1
    PROFª: LARISSA FALCÃO

    ALUNOS:
    DIEGO DE OLIVEIRA CLEMENTE
    EVANDSON CÉSAR DE ARRUDA SANTIAGO
    GABRIEL JOSÉ FRAGOSO DE SOUZA LIMA
    TARCISIO FONSECA DA SILVA TELES

    PROJETO: JOGO DOTS AND BOXES.
---------------------------------------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <windows.h>
#include <conio.h>

int main() //  por enquanto chama, funcao "inicio" e depois "menu"
{
    //inicio();
    menu();
    return 0;
}
