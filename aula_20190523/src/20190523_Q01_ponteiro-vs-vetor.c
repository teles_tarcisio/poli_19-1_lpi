/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
//      PARTE 04 DOS SLIDES, A PARTIR DO SLIDE 236
//
/* LISTA DE EXERCICIOS DE 23/05/2019: Q01
    Faça uma função que tem como parâmetros um ponteiro para um vetor de inteiros e
    um inteiro N. Essa função deve retornar a quantidade de números primos entre 0 e
    N, e deve armazenar no vetor passado como parâmetro os primos encontrados.
    Teste a sua função no método main.
    Obs1: Não pode ser usada a notação vetor[i].
    Obs2: O vetor tem um tamanho máximo de 100.
*/
#include<stdio.h>

// Versão com Vetor
/*void main()
{
    int nums[ ] = {1, 4, 8};
    for (int cont = 0; cont < 3; cont++ )
    {
        printf("%d \n", nums[cont]);
    }

    printf("\n\n qqr tecla para encerrar...");
    fflush(stdin);
    getchar();
    return;
}*/

//   Versão com Ponteiro
void main()
{
    int nums[ ] = {1, 4, 8};
    int cont;
    printf("\n incremento de ponteiro COM o \" *\" (conteudo):\n");
    for (cont = 0; cont < 3; cont++)
    {
        printf(" %d\n", *(nums + cont));
    }
    printf("\n incremento de ponteiro SEM o \" *\" (endereco de memoria):\n");
    for (cont = 0; cont < 3; cont++)
    {
        printf(" %d \n", (nums + cont));
    }
    printf("\n a diferenca entre os valores dos enderecos mostrados acima \n");
    printf("deve ser de 04 unidades, equivalente a 4 bytes (tamanho do int)");
    printf("\n\n qqr tecla para encerrar.");
    fflush(stdin);
    getchar();
    return;
}

