/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* ATIVIDADE:
    Faca um algoritmo que leia um numero de 4 digitos e escreva-o invertido.
    p. ex.: numero lido 2548, resultado exibido 8452
*/
//---------------------------------------------------------------------------------
#include <stdio.h>

// lembrar do condicional para manter o numero com 4 digitos, nem mais nem menos (opcao);
// pode usar scanf para ler o inteiro (%d);

void main()
{
    int numero = 0;         // variavel que recebera numero fornecido pelo usuario.
    int espelhado = 0;      // variavel que contera o numero escrito invertido.
    printf("Digite um numero de 04 (quatro) digitos: ");
    scanf("%d", &numero);
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro).
    */
    if ((numero < 1000) || (numero > 9999))
    {
        printf("O numero (inteiro) deve ser entre 1000 e 9999!\n");
        printf("O programa sera encerrado.\n\n");
        getchar();
        return;
    }
    int digitos[4] = {0, 0, 0, 0};  // vetor de inteiros que recebera cada digito
    /*printf("teste vetor digitos: ");
    for (int k = 0; k <= 3; k++)
    {
        printf("%d ", digitos[k]);
    }*/
    // printf("\nseparando o numero:\n");
    /* as divisoes por "modulo" a seguir separam cada digito do numero fornecido.
       como cada elemento do vetor "digitos" e' inteiro, a parte decimal da divisao
       e' simplesmente descartada pelo programa
    */
    digitos[0] = numero/1000;
    // printf("milhar = %d\n", digitos[0]);
    digitos[1] = numero%1000;
    digitos[1] = digitos[1]/100;
    // printf("centena = %d\n", digitos[1]);
    digitos[2] = numero%100;
    digitos[2] = digitos[2]/10;
    // printf("dezena = %d\n", digitos[2]);
    digitos[3] = numero%10;
    // printf("unidade = %d\n", digitos[3]);
    /*printf("vetor digitos atualizado: ");
    for (int k = 0; k <= 3; k++)
    {
        printf("%d ", digitos[k]);
    }*/
    printf("\nO numero fornecido foi: %d\n", numero);
    // a variavel "espelhado" recebe agora o numero escrito invertido
    espelhado = digitos[3]*1000 + digitos[2]*100 + digitos[1]*10 + digitos[0];
    printf("O numero escrito invertido e': %d\n\n", espelhado);
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro).
    */
    getchar();
    return;
}
