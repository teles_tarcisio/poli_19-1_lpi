/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
//  PROJETO DE DISCIPLINA: JOGO "DOTS"

/*
  MÓDULO DE IMPRESSÃO DE LINHAS DO CAMPO DE JOGO
*/

#include <stdio.h>
#define DIMENSAO 20
void main() //void print_lines(recebe argumentos?)
{
  for (unsigned short int i = 0; i < DIMENSAO; i++)
  {
    for (unsigned short int j = 0; j < DIMENSAO; j++)
    {
      printf(" %hu%hu ", i, j);
    }
    printf("\n");
  }
  fflush(stdin);
  //qqr tecla para encerrar
  getchar();
  return;
}
