/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 30/04/2019: Q05
    Criar um algoritmo que leia os elementos de uma matriz inteira 10 x 10 e escreva os
    elementos da diagonal principal.
*/

#include <stdio.h>

int matriz[10][10];

void main()
{
    /*
    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j < 10; j++)
        {
            matriz[i][j] = ((i*10)+j);
        }
    }
    */

    printf("\ntestando preenchimento da matriz:\n");
    for(int i = 0; i < 10; i++)
    {
        printf("\nLinha %d -> ", i);
        for(int j = 0; j < 10; j++)
        {
            printf(" %d%d ", i,j);
        }
    }

    for(int i = 0; i < 10; i++) //print diagonal principal
    {
        printf("\nLinha %d -> ", i);
        for(int j = 0; j < 10; j++)
        {
            if ((i == j) || ((i+j)==9))
            {
                printf(" %d%d ", i,j);
            }
            else
            {
                printf("    ");
            }
        }
    }
    printf("\n\n");

    /*
    for(int i = 0; i < 10; i++) //print diagonal secundaria
    {
        printf("\nLinha %d -> ", i);
        for(int j = 0; j < 10; j++)
        {
            if ((i+j) == 9)
            {
                printf(" %d%d ", i, j);
            }
            else
            {
                printf("    ");
            }
        }
    }
    */





    printf("\nQqr tecla para encerrar");
    getchar();
    return;

}
