/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS 01 (27/03/2019): Q02
    Criar um algoritmo que leia o valor de um deposito e o valor da taxa de juros.
    Calcular e imprimir o valor do rendimento e o valor total depois do rendimento.
    (necessario pedir o tempo para calcular o rendimento!)
*/
//---------------------------------------------------------------------------------
#include <stdio.h>

void main()
{
    // variaveis locais: valor depositado, taxa de juros, valor a ser resgatado, valor dos juros
    float deposito, taxa, valor_final, juros;
    deposito = taxa = valor_final = juros = 0;
    // variavel local, tempo (em meses) da aplicacao
    int tempo = 0;
    
    printf("LISTA01_Q02_DEPOSITO-JUROS-RENDIMENTO\n\n");
    printf("Entre com o valor depositado: R$ ");
    scanf("%f", &deposito);
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro)
    */
    if (deposito <= 0 )
    {
        printf("O deposito deve ter valor MAIOR que 0 [zero] !");
        printf("\nPressione qualquer tecla para encerrar o programa.");
        getchar();
        return;
    }

    printf("\nEntre com a taxa de juros mensal, em percentual: ");
    scanf("%f", &taxa);
    fflush(stdin);
    if (taxa <= 0 )
    {
        printf("A taxa de juros deve ter valor MAIOR que 0 [zero]%% !");
        printf("\nPressione qualquer tecla para encerrar o programa.");
        getchar();
        return;
    }

    printf("\nEntre com o tempo da aplicacao, em meses: ");
    scanf("%d", &tempo);
    fflush(stdin);
    if (tempo < 1 )
    {
        printf("O tempo minimo para resgate da aplicacao e' de 1 (um) mes.");
        printf("\nPressione qualquer tecla para encerrar o programa.");
        getchar();
        return;
    }

    printf("\nDeposito informado: R$ %.2f", deposito);
    printf("\nTaxa de juros simples informada: %.2f%% a.m.", taxa);
    printf("\nPeriodo da aplicacao: %.1d  meses", tempo);
    valor_final = deposito * (1 + ((taxa/100)*tempo));      // calculo do rendimento com juros simples 
    printf("\n\nO valor final a ser resgatado ao fim do(s) %.1d mes(es) e' de R$ %.2f", tempo, valor_final);
    juros = valor_final - deposito;
    printf("\nO valor total do rendimento sobre o deposito inicial e' de R$ %.2f", juros);
    printf("\n\nPressione qualquer tecla para encerrar o programa.");
    getchar();
    return;
}

