/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS 01 (27/03/2019): Q01
    Para varios tributos, a base de calculo e o salario minimo. Fazer um algoritmo
    que leia o valor do salario minimo e o valor do salario de uma pessoa.
    Calcular e imprimir quantos salarios minimos ela ganha.
*/
//---------------------------------------------------------------------------------
#include <stdio.h>

void main()
{
    float sal_minimo = 0;
    float sal_funcionario = 0;
    float proporcao = 0;
    printf("LISTA01_Q01_SALARIOxTRIBUTOS\n\n");
    printf("Entre com o valor do salario minimo: R$ ");
    scanf("%f", &sal_minimo);
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro).
    */
    if (sal_minimo <= 0 )
    {
        printf("O salario minimo deve ser MAIOR que 0 [zero] !\nPressione qualquer tecla para encerrar o programa.");
        getchar();
        return;
    }

    printf("\nEntre com o valor do salario do funcionario: R$ ");
    scanf("%f", &sal_funcionario);
    fflush(stdin);
    if (sal_funcionario <= 0 )
    {
        printf("O salario do funcionario deve ser MAIOR que 0 [zero] !");
        printf("\nPressione qualquer tecla para encerrar o programa.");
        getchar();
        return;
    }

    printf("\nSalario minimo informado: R$ %.2f", sal_minimo);
    printf("\nSalario do funcionario informado: R$%.2f", sal_funcionario);
    proporcao = sal_funcionario / sal_minimo;       // calcula razao entre salario do funcionario e salario minimo
    printf("\nO salario do funcionario equivale a %.2f salarios minimos\n\n", proporcao);
    printf("\nPressione qualquer tecla para encerrar o programa.");
    getchar();
    return;
}

