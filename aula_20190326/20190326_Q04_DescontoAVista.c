/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS 01 (27/03/2019): Q04
    Em épocas de pouco dinheiro, os comerciantes estão procurando aumentar suas
    vendas oferecendo desconto. Faça um algoritmo que possa entrar com o valor de
    um produto e imprima o novo valor tendo em vista que o desconto foi de 9%.
*/
//---------------------------------------------------------------------------------
#include <stdio.h>

void main()
{
    float valor_produto = 0;
               
    printf("LISTA01_Q04_DESCONTO-LOJA\n\n");
    printf("Entre com o valor do produto desejado: R$ ");
    scanf("%f", &valor_produto);
    fflush(stdin);
    if ( valor_produto <= 0 )
    {
        printf("O valor do produto deve ser maior que R$ 0.00 !");
        printf("\nPressione qualquer tecla para encerrar o programa.");
        getchar();
        return;
    }
    
    printf("\n\nValor do produto: R$ %.2f", valor_produto);
    printf("\n** Pagamento no cartao de credito, ate' 10x sem juros, parcela minima de R$ 40.00");
    printf("\n\n>> Com desconto de 9%% para pagamento 'a vista (em especie ou no debito): R$ %.2f <<", (valor_produto*0.91));
    printf("\n>> Para pagamento 'a vista voce economiza: R$ %.2f <<", (valor_produto*0.09));

    printf("\n\nPressione qualquer tecla para encerrar o programa.");
    getchar();
    return;
}

