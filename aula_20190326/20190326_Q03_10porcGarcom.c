/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS 01 (27/03/2019): Q03
    Todo restaurante, embora por lei nao possa obrigar o cliente a pagar, cobra 10%
    para o garçom. Fazer um algoritmo que leia o valor gasto com despesas realizadas
    em um restaurante e imprima o valor total com a gorjeta.
*/
//---------------------------------------------------------------------------------
#include <stdio.h>

void main()
{
    float valor_conta = 0;
    int qtde = 0;       // numero de pessoas na mesa do restaurante
            
    printf("LISTA01_Q03_RESTAURANTE-10%-GARCOM\n\n");
    printf("Entre com o valor da conta da mesa: R$ ");
    scanf("%f", &valor_conta);
    fflush(stdin);
    if ( valor_conta <= 0 )
    {
        printf("O valor da conta deve ser maior que R$ 0.00 !");
        printf("\nPressione qualquer tecla para encerrar o programa.");
        getchar();
        return;
    }
    
    printf("Entre com a quantidade de pessoas na mesa: ");
    scanf("%d", &qtde);
    fflush(stdin);
    if ( qtde < 1)
    {
        printf("A quantidade minima de pessoas na mesa e' 1 (uma).");
        printf("\nPressione qualquer tecla para encerrar o programa.");
        getchar();
        return;
    }
    
    printf("\n\nValor total da nota: R$ %.2f", valor_conta);
    printf("\n\nQuantidade de pessoas na mesa: %d", qtde);
    
    printf("\nValor da conta por pessoa, sem gorjeta: R$ %.2f", (valor_conta/qtde));
    
    printf("\n\nValor da nota com gorjeta de 10%%: R$ %.2f", (valor_conta*1.1));
    printf("\nValor da conta por pessoa, 10%% de gorjeta: R$ %.2f", ((valor_conta*1.1)/qtde));

    printf("\n\nValor da nota com gorjeta de 15%%: R$ %.2f", (valor_conta*1.15));
    printf("\nValor da conta por pessoa, 15%% de gorjeta: R$ %.2f", ((valor_conta*1.15)/qtde));

    printf("\n\nValor da nota com gorjeta de 20%%: R$ %.2f", (valor_conta*1.2));
    printf("\nValor da conta por pessoa, 20%% de gorjeta: R$ %.2f", ((valor_conta*1.2)/qtde));
    
    printf("\n\n -- Exija o cupom fiscal -- ");

    printf("\n\nPressione qualquer tecla para encerrar o programa.");
    getchar();
    return;
}

