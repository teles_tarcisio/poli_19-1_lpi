/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 07/05/2019: Q02
    Implemente um programa em C que leia o nome, a idade e o endereço
    de uma pessoa e	armazene os dados em uma estrutura.
*/

#include <stdio.h>
#include <string.h>
#define QTDE 1

struct info_pessoa
{
    char nome[50];  // nome com no maximo 49 caracteres ('\0') no final
    unsigned short int idade;   //  obter/exibir com %hu
    char end_logradouro[30];    // logradouro com no maximo 29 caracteres
    unsigned short int end_numero;
    char end_bairro[30];    // bairro com no maximo 29 caracteres
    char end_cidade[30];    // cidade com no maximo 29 caracteres
    char end_UF[3];     // UF com até dois carateres (PE, RN, etc)
    char end_CEP[9];    // CEP sem o hifen
};

void main()
{
    printf("\n\t\t>> Cadastro Nome-Idade-Endereco <<\n\n");

    struct info_pessoa dados_pessoas[QTDE];
    // usar vetor aqui acima é "desnecessario", mas com vetor dá para generalizar depois
    printf("\nForneca o nome completo da pessoa (max 49 caracteres): ");
    gets(dados_pessoas[0].nome);
    fflush(stdin);

    do
    {
        printf("\nForneca a idade (a partir de 1), em anos: ");
        scanf("%d", &dados_pessoas[0].idade);
    } while (dados_pessoas[0].idade < 1);
    fflush(stdin);

    printf("\nEndereco - forneca o logradouro (Rua, Av., etc): ");
    gets(dados_pessoas[0].end_logradouro);
    fflush(stdin);
    do
    {
        printf("\nEndereco - forneca o numero (0 'zero' para sem numero): ");
        scanf("%hu", &dados_pessoas[0].end_numero);
    } while (dados_pessoas[0].end_numero < 0);
    fflush(stdin);

    printf("\nEndereco - forneca o bairro: ");
    gets(dados_pessoas[0].end_bairro);
    fflush(stdin);
    printf("\nEndereco - forneca a cidade: ");
    gets(dados_pessoas[0].end_cidade);
    fflush(stdin);
    printf("\nEndereco - forneca o estado (UF): ");
    gets(dados_pessoas[0].end_UF);
    fflush(stdin);
    printf("\nEndereco - forneca o CEP (somente numeros): ");
    gets(dados_pessoas[0].end_CEP);
    fflush(stdin);

    printf("\n\nConfira os dados fornecidos: ");
    printf("\nNome: %s", dados_pessoas[0].nome);
    printf("\nIdade: %hu anos", dados_pessoas[0].idade);
    printf("\nEndereco completo: ");
    printf("\n%s, nro. %hu", dados_pessoas[0].end_logradouro, dados_pessoas[0].end_numero);
    printf("\n%s, %s, %s", dados_pessoas[0].end_bairro, dados_pessoas[0].end_cidade, dados_pessoas[0].end_UF);
    printf("\nCEP ");
    for (int i = 0; i < 5; i++)
    {
        printf("%c", dados_pessoas[0].end_CEP[i]);
    }
    printf("-");
    for (int i = 5; i < 9; i++)
    {
        printf("%c", dados_pessoas[0].end_CEP[i]);
    }

    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
