/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 07/05/2019: Q01 >>STRUCTS<< !!!!
    Escrever um programa que cadastre o nome, a matrícula e duas notas de vários alunos.
	Em seguida imprima a matrícula, o nome e a média de cada um deles.
*/

#include <stdio.h>
#include <string.h>
#define QTDE 2

struct info_aluno
{
    char nome[50];  // nome com no maximo 49 caracteres ('\0') no final
    char matricula[12];  // matricula com maximo de 11 caracteres (pra ficar igual a CPF)
    float nota1;
    float nota2;
};

//  unsigned int, 16 bits, [0...65535], ler/exibir com %u
//  const unsigned int quantidade = 0;

void main()
{
    printf("\n\t\t>> Sistema de Notas - conSig@ <<\n\n");

    /*
    printf("\nEntre com a quantidade de alunos desejada (min. 0, max. 65535): ");
    do
    {
        scanf("%u", &quantidade);
    } while (quantidade < 0);
    */
    //struct info_aluno alunos[quantidade];
    /* /\  /\   /\ o tamanho do vetor nao pode variar
    em tempo de execucao (sem usar funções de manipulacao de memoria),
    como fazer então?
    */

    struct info_aluno alunos[QTDE];
    for(int i = 0; i < QTDE; i++)
    {
        printf("\n\tDigite os dados do %do aluno:", i+1);
        printf("\nNome: ");
        gets(alunos[i].nome);   //atencao ao maximo de caracteres
        printf("\nMatricula: ");
        gets(alunos[i].matricula);   //atencao ao maximo de caracteres
        printf("\nNota 1o EE: ");
        scanf("%f", &alunos[i].nota1);
        printf("\nNota 2o EE: ");
        scanf("%f", &alunos[i].nota2);
        printf("--------------------");
        fflush(stdin);
    }

    printf("\n\t\t>> RELACAO DE ALUNOS E NOTAS <<");
    printf("\n\t| NOME \t\t| MATRICULA \t\t| NOTA_01 \t\t| NOTA_02 \t\t| MEDIA\n");
    for (int i = 0; i < QTDE; i++)
    {
        printf("\t  %s\t", alunos[i].nome);
        printf("\t  %s\t", alunos[i].matricula);
        printf("\t  %.2f\t", alunos[i].nota1);
        printf("\t\t  %.2f\t", alunos[i].nota2);
        printf("\t\t  %.2f\n", (alunos[i].nota1 + alunos[i].nota2)/2);
    }

    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
