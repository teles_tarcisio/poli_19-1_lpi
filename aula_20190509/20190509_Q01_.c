/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 09/05/2019: Q01
    Peça ao usuário para digitar seus dados pessoais (Nome, Endereço,
    Data de Nascimento, Cidade, CEP, email), verifique se as informações de
    Data de Nascimento, CEP e email fazem sentido, e mostre ao usuário as informações,
    se estão todas corretas, ou  mostre que alguma informação estava errada.
*/

#include <stdio.h>
#include <string.h>

struct Cadastro_usuario
{
    char nome[50];  // nome com no maximo 49 caracteres ('\0') no final
    unsigned short int nasc_data;   // na prox "versao", conferir bissextos e etc!!
    unsigned short int nasc_mes;    // na prox "versao", conferir bissextos e etc!!
    unsigned short int nasc_ano;    // conferir se ano > 0
    char end_logradouro[30];    // logradouro com no maximo 29 caracteres
    unsigned short int end_numero;
    char end_bairro[30];    // bairro com no maximo 29 caracteres
    char end_cidade[30];    // cidade com no maximo 29 caracteres
    char end_UF[3];     // UF com até dois carateres (PE, RN, etc)
    char end_CEP[9];    // CEP sem o hifen
    char e_mail[52]     // endereco de e-mail com maximo de 21 caracteres
};

void main()
{
    struct Cadastro_usuario cadastro_01;
    printf("\n\n >> CADASTRO DE USUARIOS (v0.2) <<");
    printf("\nForneca o nome completo da pessoa (max 49 caracteres): ");
    gets(cadastro_01.nome);
    fflush(stdin);

    printf("\nEndereco - forneca o logradouro (Rua, Av., etc): ");
    gets(cadastro_01.end_logradouro);
    fflush(stdin);

    do
    {
        printf("\nEndereco - forneca o numero (0 'zero' para sem numero): ");
        scanf("%hu", &cadastro_01.end_numero);
    } while (cadastro_01.end_numero < 0);
    fflush(stdin);

    printf("\nEndereco - forneca o bairro: ");
    gets(cadastro_01.end_bairro);
    fflush(stdin);
    printf("\nEndereco - forneca a cidade: ");
    gets(cadastro_01.end_cidade);
    fflush(stdin);
    printf("\nEndereco - forneca o estado (UF): ");
    gets(cadastro_01.end_UF);
    fflush(stdin);
    do
    {
        printf("\nEndereco - forneca um CEP valido (somente numeros): ");
        gets(cadastro_01.end_CEP);/* code */
    } while (strlen(cadastro_01.end_CEP) < 8);
    fflush(stdin);

    do
    {
        printf("\nNascimento - forneca o DIA de nascimento [01 a 31]: ");
        scanf("%hu", &cadastro_01.nasc_data);
    } while ( (cadastro_01.nasc_data < 1) || (cadastro_01.nasc_data > 31) );
    fflush(stdin);

    do
    {
        printf("\nNascimento - forneca o MES de nascimento [01 a 12]: ");
        scanf("%hu", &cadastro_01.nasc_mes);
    } while ( (cadastro_01.nasc_mes < 1) || (cadastro_01.nasc_mes > 12) );
    fflush(stdin);

    do
    {
        printf("\nNascimento - forneca o ANO de nascimento [0001 a 2019]: ");
        scanf("%hu", &cadastro_01.nasc_ano);
    } while ( (cadastro_01.nasc_ano < 1) || (cadastro_01.nasc_ano > 2019) );
    fflush(stdin);

    unsigned short int arroba = 0, ponto = 0; //flags de teste
    do
    {
        printf("\nForneca um endereco de e-mail valido [nome@dominio.xx]: ");
        gets(cadastro_01.e_mail);
        for (int k = 0; k <= strlen(cadastro_01.e_mail); k++)
        {
            // antes do @, forçar no minimo 3 caracteres
            if (( cadastro_01.e_mail[k] == '@') && (k < 3) )
            {
                arroba = 0;
            }
            else if (( cadastro_01.e_mail[k] == '@') && (k >= 3) )
            {
                arroba = 1;
            }
            // no mínimo 3 caracteres entre '@' e '.';
            else if (( cadastro_01.e_mail[k] == '.') && (k < 7) && (arroba == 1) )
            {
                ponto = 0;
            }
            else if (( cadastro_01.e_mail[k] == '.') && (k >= 7) && (arroba == 1) )
            {
                ponto = 1;
            }
        }
    } while ((arroba < 1) && (ponto < 1));
    fflush(stdin);

    //conferir como validar o e-mail!!!!) <<<<< testando em 20190517, 10:45

    printf("\n\nConfira os dados fornecidos: ");
    printf("\nNome: %s", cadastro_01.nome);
    printf("\nData de Nascimento: %hu / %hu / %hu", cadastro_01.nasc_data,
            cadastro_01.nasc_mes, cadastro_01.nasc_ano);
    printf("\nEndereco completo: ");
    printf("\n%s, nro. %hu", cadastro_01.end_logradouro , cadastro_01.end_numero);
    printf("\n%s, %s, %s", cadastro_01.end_bairro , cadastro_01.end_cidade, cadastro_01.end_UF);
    printf("\nCEP ");
    for (int i = 0; i < 5; i++)
    {
        printf("%c", cadastro_01.end_CEP[i]);
    }
    printf("-");
    for (int i = 5; i < 9; i++)
    {
        printf("%c", cadastro_01.end_CEP[i]);
    }

    printf("E-mail: %s", cadastro_01.e_mail);

    fflush(stdin);
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
