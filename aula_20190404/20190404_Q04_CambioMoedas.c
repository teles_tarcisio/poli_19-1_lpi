/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 04/04/2019: Q04
    Fazer um algoritmo que possa converter uma determinada quantia em reais para
    uma das seguintes moedas: f - franco suíço; l - libra esterlina; d - dólar; e - euro.
*/

#include <stdio.h>

char moeda;
float real = 0, convertido = 0;
// cotacao franco suico 05/04/2019 : CHF 1.00 <-> BRL 3.86
// cotacao libra esterlina 05/04/2019: GBP 1.00 <-> BRL 5.03
// cotacao dolar americano 05/04/2019: USD 1.00 <-> BRL 3.86
// cotacao euro 05/04/2019: EUR 1.00 <-> BRL 4.33

void main()
{
    printf("\nEntre com a quantia em reais a ser convertida: R$ ");
    scanf("%f", &real);
    fflush(stdin);  // limpar o stdin para nao pular o getchar

    printf("\nEscolha para qual moeda deseja converter: ");
    printf("\n-> [D]olar Americano (USD)");
    printf("\n-> [L]ibra Esterlina (GBP)");
    printf("\n-> [F]ranco Suico (CHF)");
    printf("\n-> [E]uro (EUR) ");
    moeda = getchar();

    switch (moeda)
    {
        case 'D':
        case 'd':
            convertido = real/(3.86);
            printf("\n\nA quantia de R$ %.2f e' equivalente a USD %.2f", real, convertido);
            break;

        case 'L':
        case 'l':
            convertido = real/(5.03);
            printf("\n\nA quantia de R$ %.2f e' equivalente a GBP %.2f", real, convertido);
            break;

        case 'F':
        case 'f':
            convertido = real/(3.86);
            printf("\n\nA quantia de R$ %.2f e' equivalente a CHF %.2f", real, convertido);
            break;

        case 'E':
        case 'e':
            convertido = real/(4.33);
            printf("\n\nA quantia de R$ %.2f e' equivalente a EUR %.2f", real, convertido);
            break;
        default:
            printf("\n\nEscolha uma opcao valida, o programa sera' encerrado");
            break;
    }

    fflush(stdin);
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
