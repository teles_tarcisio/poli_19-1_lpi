/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 04/04/2019: Q02
    A prefeitura do Rio de Janeiro abriu uma linha de crédito para os funcionários
    estatutários. O valor máximo da prestação não poderá ultrapassar 30% do salário 
    bruto. Fazer um algoritmo que permita entrar com o salário bruto e o valor da 
    prestação e informar se o empréstimo pode ou não ser concedido. 
*/

#include <stdio.h>

float salario = 0, parcela = 0;

void main()
{
    printf("\nEntre com o valor do salario bruto do funcionario: R$ ");
    scanf("%f", &salario);
       
    printf("\nEntre com o valor da parcela do emprestimo: R$ ");
    scanf("%f", &parcela);
    fflush(stdin);  // limpar o stdin para nao pular o getchar
    
    printf("\nSalario bruto informado R$ %.2f", salario);
    printf("\nValor da parcela informado R$ %.2f", parcela);

    if( (parcela <= salario*0.3) )
    {
        printf("\nO valor maximo possivel da parcela ");
        printf("para o salario bruto informado e' R$ %.2f", (salario*0.3));
        printf("\n\nO emprestimo pode ser concedido normalmente.");
    }
    else if( (parcela > salario*0.3) )
    {
        printf("\nO valor da parcela informado excede o valor maximo ");
        printf("possivel por parcela para o salario bruto informado (R$ %.2f)", (salario*0.3));
        printf("\n\nO emprestimo nao pode ser concedido nestes termos.");
    }
    
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}