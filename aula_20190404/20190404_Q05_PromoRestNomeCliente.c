/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 04/04/2019: Q05
    Um restaurante faz uma promoção semanal de descontos para clientes de acordo
    com as iniciais do nome da pessoa. Criar um algoritmo que leia o primeiro nome do
    cliente, o valor de sua conta e se o nome iniciar com as letras
    A, D, M ou S, dar um desconto de 30%.
    Para um cliente cujo nome não se inicia por nenhuma dessas 
    letras, exibir a mensagem 
    "Que pena. Nesta semana o desconto não é para seu nome;
    mas continue nos prestigiando que sua vez chegará".
*/

#include <stdio.h>
#include <string.h>

char nome[50] = " ";     //comprimento maximo do nome do cliente e' 50 caracteres (definido)
void main()
{
    printf("\n ** Promocao da Semana **");
    printf("\nSeja bem vindo(a)! Forneca seu nome: ");
    gets(nome);
    
    fflush(stdin);  // limpar o stdin para nao pular o getchar
    
    switch (nome[0])
    {
        case 'A':
        case 'a':
            printf("\n\nParabens %s !!", nome);
            printf("\nNesta semana no restaurante voce ganha desconto de 30%% no valor da conta!");
            break;
        
        case 'D':
        case 'd':
            printf("\n\nParabens %s !!", nome);
            printf("\nNesta semana no restaurante voce ganha desconto de 30%% no valor da conta!");
            break;

        case 'M':
        case 'm':
            printf("\n\nParabens %s !!", nome);
            printf("\nNesta semana no restaurante voce ganha desconto de 30%% no valor da conta!");
            break;
        
        case 'S':
        case 's':
            printf("\n\nParabens %s !!", nome);
            printf("\nNesta semana no restaurante voce ganha desconto de 30%% no valor da conta!");
            break;

        default:
            printf("\n\nQue pena. Nesta semana o desconto nao e para seu nome...");
            printf("\nMas continue nos prestigiando que sua vez chegara!");
            break;
    }
        
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}a