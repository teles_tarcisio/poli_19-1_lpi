/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 04/04/2019: Q01
    Entrar com um número e informar se ele é divisível por 10, por 5, por 2 ou se não é 
    divisível por nenhum destes. 
*/

#include <stdio.h>

int num = 0;

void main()
{
    printf("\nEntre com o numero a ser conferido: ");
    scanf("%d", &num);
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */

    if( (num%2 == 0) && (num%5 == 0) )
    {
        printf("\nO numero %d informado e' divisivel por 2 e por 5, portanto tambem e' divisivel por 10", num);
    }
    else if( num%2 == 0 )
    {
        printf("\nO numero %d informado e' divisivel por 2", num);
    }
    else if( num%5 == 0 )
    {
        printf("\nO numero %d informado e' divisivel por 5", num);
    }
    else
    {
        printf("\nO numero %d informado nao e' divisivel por 2, por 5 ou por 10", num);
    }

    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}