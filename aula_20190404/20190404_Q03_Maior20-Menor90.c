/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 04/04/2019: Q03
    Construir um algoritmo que indique se o número digitado está compreendido entre 
    20 e 90 ou não. 
*/

#include <stdio.h>

float numero = 0;

void main()
{
    printf("\nEntre com o numero a ser comparado: ");
    scanf("%f", &numero);
    fflush(stdin);  // limpar o stdin para nao pular o getchar
    
    if( (numero < 20 ) )
    {
        printf("\n\nO numero %.2f informado e' menor que 20", numero);
    }
    else if( (numero > 90) )
    {
        printf("\n\nO numero %.2f informado e' maior que 90", numero);
    }
    else // 20 <= numero <= 90
    {
        printf("\n\nO numero %.2f informado esta' entre 20 e 90", numero);
    }
    
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}