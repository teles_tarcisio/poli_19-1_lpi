/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* EXERCICIO EM SALA (02/04/2019), Q01:
Faça algoritmo que leia três números e determine quantos são iguais.
*/

#include <stdio.h>

int a = 0, b = 0, c = 0;

void main()
{
    printf("\nEntre com o 1o numero: a = ");
    scanf("%d", &a);
    printf("\nEntre com o 2o numero: b = ");
    scanf("%d", &b);
    printf("\nEntre com o 1o numero: c = ");
    scanf("%d", &c);

    printf("\n\nVoce informou os numeros: a= %d  ,  b= %d  e  c= %d", a, b, c);

    if ((a==b) && (a==c))
    {
        printf("\nOs tres numeros informados sao iguais.");
    }
    else if (a==b)
    {
        printf("\nOs numeros 'a' e 'b' sao iguais.");
    }
    else if (a==c)
    {
        printf("\nOs numeros 'a' e 'c' sao iguais.");
    }
    else if (b==c)
    {
        printf("\nOs numeros 'b' e 'c' sao iguais.");
    }
    else
    {
        printf("\nTodos os numeros informados sao diferentes.");
    }

    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro).
    */
    printf("\n\nPressione qualquer tecla para encerrar o programa...");
    getchar();
    return;

}
