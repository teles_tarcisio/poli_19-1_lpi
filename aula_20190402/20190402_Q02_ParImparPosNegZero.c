/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* EXERCICIO EM SALA (02/04/2019), Q02:
Faça um algoritmo capaz de identificar
se um número é: a) par ou ímpar; b) positivo, negativo ou zero
*/

#include <stdio.h>

int numero = 0;

void main()
{
    printf("\nEntre com o numero a ser testado: ");
    scanf("%d", &numero);
    /*
    Os testes de numeros pares e impares foram aninhados com os de negativo e positivo
    pois fazendo primeiro os testes de numero par ou impar, os testes de positivo
    e negativo eram ignorados pelo programa.
    */

    if (numero > 0) //numero positivo
    {
        if( (numero%2 == 0) ) //positivo e par
        {
            printf("\nO numero %d informado e' POSITIVO e PAR", numero);   
        }
        else //positivo e impar
        {
            printf("\nO numero %d informado e' POSITIVO e IMPAR", numero);
        }
    }
    else if ( numero < 0 )
    {
        if( (numero%2 == 0) ) //negativo e par
        {
            printf("\nO numero %d informado e' NEGATIVO e PAR", numero);   
        }
        else //negativo e impar
        {
            printf("\nO numero %d informado e' NEGATIVO e IMPAR", numero);
        }
    }
    else
    {
        printf("\nO numero %d informado e' 0 (zero)", numero);
    }

    printf("\n\nPressione qualquer tecla para encerrar o programa...");
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro).
    */
    getchar();
    return;
}
