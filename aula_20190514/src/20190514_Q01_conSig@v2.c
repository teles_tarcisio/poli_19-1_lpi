/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 14/05/2019:
    Escreva uma função que determine a média e a situação de um aluno em uma disciplina.
    A função recebe como parâmetros as três notas de um aluno (p1, p2, e p3),
    seu número de faltas (faltas), o número total de aulas da disciplina (aulas)
    e o ponteiro para uma variável (media), conforme o seguinte protótipo:
    char situacao(float p1, float p2, float p3, int faltas, int aulas, float *media);

    Na variável indicada pelo ponteiro media, a função deve armazenar a média do aluno,
    calculada como a média aritmética das três provas.
    Além disso, a função deve retornar um caractere indicando a situação do aluno no curso,
    definido de acordo com o critério da tabela em anexo.

    Em seguida, escreva a função principal de um programa que utiliza
    a função anterior para determinar a situação de um aluno.

    O programa deve:
	--> Ler do teclado três números reais e dois números inteiros,
		representando as notas da p1, p2 e p3, o número de faltas
		e o número de aulas, respectivamente;
	--> Chamar a função desenvolvida na primeira questão para
		determinar a média e a situação do aluno na disciplina;
	--> Exibir a média (com apenas uma casa decimal) e a situação
		do aluno, isto é, “APROVADO”, “REPROVADO” ou “REPROVADO POR FALTAS”,
		dependendo do caractere retornado pela função, conforme a tabela em anexo.Faça um algoritmo que calcule a soma dos N primeiros números primos,
    sendo N definido pelo usuário no módulo principal.
    O algoritmo ainda deverá ter os módulos Soma_Primos e Primo,
    sendo que o primeiro será responsável pela soma dos números que forem primos e
    o segundo será responsável por verificar se o número em questão é primo ou não.
*/

#include <stdio.h>
//#include <string.h>

/*
    Para que o retorno da função seja REALMENTE uma string, usar
    const char *situacao(argumentos)
*/
char situacao(float p1, float p2, float p3, int faltas, int aulas, float *media)
{
    *media = (p1 + p2 + p3)/3;
    if( (faltas <= aulas*(0.25)) && (*media >=6.0) )
    {
        return 'A';
    }
    else if( (faltas <= aulas*(0.25)) && (*media < 6.0) )
    {
        return 'R';
    }
    else if(faltas > aulas*(0.25))
    {
        return 'F';
    }
}

void main()
{
    float PV[3] = {0, 0, 0};
    printf("\n\n >> SISTEMA DE NOTAS conSig@ v2 <<");
    for (int i = 0; i < 3; i++)
    {
        printf("\nForneca a nota da PV%d : ", (i+1));
        scanf("%f", &PV[i]);
    }
    fflush(stdin);
    /*
    !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
    isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
    encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */

    int carga_horaria = 0, qtde_faltas = 0;
    printf("\nForneca a carga horaria total da disciplina, em horas: ");
    scanf("%d", &carga_horaria);
    printf("\nForneca a quantidade de faltas (1 falta = 1 hora-aula): ");
    scanf("%d", &qtde_faltas);
    fflush(stdin);

    float media_geral = 0;
    // situacao(float p1, float p2, float p3, int faltas, int aulas, float *media);
    // caso o retorno precise ser uma STRING, a propria funcao deve ser declarada como ponteiro-char;
    float *pntr_media = &media_geral;
    char result_final = situacao(PV[0], PV[1], PV[2], qtde_faltas, carga_horaria, pntr_media);
    switch (result_final)
    {
    case 'A':
    case 'a':
        printf("\nResultado final: APROVADO, ");
        break;
    case 'R':
    case 'r':
        printf("\nResultado final: REPROVADO, ");
        break;
    case 'F':
    case 'f':
        printf("\nResultado final: REPROVADO POR FALTA, ");
        break;
    default:
        break;
    }
    printf("Media geral: %.1f", media_geral);

    fflush(stdin);
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
