/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 23/04/2019: Q03
    Criar um algoritmo que receba um número que corresponda a um mês do 1º
    trimestre e escreva o mês correspondente;
    caso o usuário digite o número fora do intervalo deverá aparecer inválido,
    mas utilizando uma função do tipo void.
*/

#include <stdio.h>
#include <stdbool.h>

void firstquarter(unsigned int x)
{
    /*
    esta função deve receber um numero maior que zero, entre 01 e 03,
    e imprimir de volta o mes correspondente (JAN/FEV/MAR)
    */
   switch (x)
   {
    case ??:    //JAN
    case 97:    //a
       return 1;
       break;   // break após o return é provavelmente inútil, mas "pelo padrão"
    case 69:    //E
    case 101:   //e
       return 1;
       break;
    case 73:    //I
    case 105:   //i
       return 1;
       break;
    case 79:    //O
    case 111:   //o
        return 1;
        break;
    case 85:    //U
    case 117:   //u
        return 1;
        break;
   default:
        return 0;
        break;
   }
}
