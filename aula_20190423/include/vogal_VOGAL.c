/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 23/04/2019: Q02
    Criar uma função que receba um caractere como parâmetro e retorne 1,
    caso seja uma vogal, minúscula ou maiúscula, e 0 caso contrário.
*/

#include <stdio.h>
#include <stdbool.h>

// char : decimal
// A:65 ; E:69 ; I:73 ; O:79 ; U:85 ; a:97 ; e:101 ; i:105 ; o:111 ; u:117 ;

bool vogal_VOGAL(char x)
{
    /*
    if( ((x >= 'a') && (x <= 'z')) || ((x >= 'A') && (x <= 'Z')) )
    {
        return 1;
    }
    else
    {
        return 0;
    }
    */
   switch (x)
   {
    case 65:    //A
    case 97:    //a
       return 1;
       break;   // break após o return é provavelmente inútil, mas "pelo padrão"
    case 69:    //E
    case 101:   //e
       return 1;
       break;
    case 73:    //I
    case 105:   //i
       return 1;
       break;
    case 79:    //O
    case 111:   //o
        return 1;
        break;
    case 85:    //U
    case 117:   //u
        return 1;
        break;
   default:
        return 0;
        break;
   }
}
