/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 23/04/2019: Q01
    Criar um algoritmo que possa entrar com três números e, para cada um,
    imprimir o dobro. Usar uma função que retorne valor.
*/

#include <stdio.h>
#include "../lib/dobrar_float.h"
#include "../include/dobrar_float.c"

float num = 0;

void main()
{
    //float dobrar_float(float x);
    for( int i = 0; i < 3; i++)
    {
        printf("\nEntre com o %do numero: ", (i+1));
        scanf("%.f", &num);
        fflush(stdin);
        /*
            !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
            isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
            encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
        */
       printf("\nO dobro de %.4f ", num);
       printf("e' %.4f", dobrar_float(num));
    }
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
