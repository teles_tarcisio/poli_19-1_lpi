/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 23/04/2019: Q02
    Criar uma função que receba um caractere como parâmetro e retorne 1,
    caso seja uma vogal, minúscula ou maiúscula, e 0 caso contrário.
*/

// char : decimal
// A:65 ; E:69 ; I:73 ; O:79 ; U:85 ; a:97 ; e:101 ; i:105 ; o:111 ; u:117 ;

#include <stdio.h>
#include "../lib/vogal_VOGAL.h"
#include "../include/vogal_VOGAL.c"

void main()
{
    char caractere = '?';   //meu debugging
    printf("\n\n>>\tEste programa confere se o caractere fornecido pelo ");
    printf("\n\tusuario e' uma vogal (minuscula ou maiuscula)");
    do
    {
        printf("\n\nEntre com o caractere a ser conferido, ");
        printf("\n(forneca * para encerrar o programa): ");
        scanf("%c", &caractere);
        fflush(stdin);
        printf("\nretorno de [bool vogal_VOGAL(%c)]: %d", caractere, vogal_VOGAL(caractere));
    } while (caractere != '*');

    //fflush(stdin);
    /*
        !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
