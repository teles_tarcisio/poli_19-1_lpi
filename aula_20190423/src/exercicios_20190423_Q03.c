/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 23/04/2019: Q03
    Criar um algoritmo que receba um número que corresponda a um mês do 1º
    trimestre e escreva o mês correspondente;
    caso o usuário digite o número fora do intervalo deverá aparecer inválido,
    mas utilizando uma função do tipo void.
*/

#include <stdio.h>
#include "../lib/firstquarter.h"
#include "../include/firstquarter.c"

void main()
{
    char caractere = '?';   //meu debugging
    printf("\n\n>>\tEste programa confere se o caractere fornecido pelo ");
    printf("\n\tusuario e' uma vogal (minuscula ou maiuscula)");
    do
    {
        printf("\n\nEntre com o caractere a ser conferido, ");
        printf("\n(forneca * para encerrar o programa): ");
        scanf("%c", &caractere);
        fflush(stdin);
        printf("\nretorno de [bool vogal_VOGAL(%c)]: %d", caractere, vogal_VOGAL(caractere));
    } while (caractere != '*');

    //fflush(stdin);
    /*
        !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}
