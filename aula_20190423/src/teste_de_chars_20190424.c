/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */

// char : decimal
// A:65 ; E:69 ; I:73 ; O:79 ; U:85 ; a:97 ; e:101 ; i:105 ; o:111 ; u:117 ;


#include <stdio.h>

void main()
{
    char caractere = '?';
    for (int i = 65; i <= 122; i++ )
    {
        caractere = i;
        printf("\n i = %d, char = %c", i, caractere);
    }


    //fflush(stdin);
    /*
        !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}

/* conferindo a saída do executavel:
 i = 65, char = A <<
 i = 66, char = B
 i = 67, char = C
 i = 68, char = D
 i = 69, char = E <<
 i = 70, char = F
 i = 71, char = G
 i = 72, char = H
 i = 73, char = I <<
 i = 74, char = J
 i = 75, char = K
 i = 76, char = L
 i = 77, char = M
 i = 78, char = N
 i = 79, char = O <<
 i = 80, char = P
 i = 81, char = Q
 i = 82, char = R
 i = 83, char = S
 i = 84, char = T
 i = 85, char = U <<
 i = 86, char = V
 i = 87, char = W
 i = 88, char = X
 i = 89, char = Y
 i = 90, char = Z
 i = 91, char = [
 i = 92, char = \
 i = 93, char = ]
 i = 94, char = ^
 i = 95, char = _
 i = 96, char = `
 i = 97, char = a <<
 i = 98, char = b
 i = 99, char = c
 i = 100, char = d
 i = 101, char = e <<
 i = 102, char = f
 i = 103, char = g
 i = 104, char = h
 i = 105, char = i <<
 i = 106, char = j
 i = 107, char = k
 i = 108, char = l
 i = 109, char = m
 i = 110, char = n
 i = 111, char = o <<
 i = 112, char = p
 i = 113, char = q
 i = 114, char = r
 i = 115, char = s
 i = 116, char = t
 i = 117, char = u <<
 i = 118, char = v
 i = 119, char = w
 i = 120, char = x
 i = 121, char = y
 i = 122, char = z
 */
