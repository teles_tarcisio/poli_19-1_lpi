/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 23/04/2019: Q03
    Criar um algoritmo que receba um número que corresponda a um mês do 1º
    trimestre e escreva o mês correspondente;
    caso o usuário digite o número fora do intervalo deverá aparecer inválido,
    mas utilizando uma função do tipo void.
*/

#include <stdio.h>
#include <stdbool.h>

/*
    esta funcao retorna 1 caso o caractere fornecido pelo
    usuário seja uma vogal (minúscula ou maiúscula), e retorna 0,
    caso contrário.
*/
void firstquarter(unsigned int x);
