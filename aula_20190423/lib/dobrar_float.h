/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 23/04/2019: Q01
    Criar um algoritmo que possa entrar com três números e, para cada um,
    imprimir o dobro. Usar uma função que retorne valor. 
*/

#include <stdio.h>

/*
    esta funcao retorna um valor do tipo float,
    que e' o dobro do valor float fornecido como entrada
    da função
*/
float dobrar_float(float x);