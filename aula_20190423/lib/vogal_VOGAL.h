/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 23/04/2019: Q02
    Criar uma função que receba um caractere como parâmetro e retorne 1,
    caso seja uma vogal, minúscula ou maiúscula, e 0 caso contrário.
*/

#include <stdio.h>
#include <stdbool.h>

/*
    esta funcao retorna 1 caso o caractere fornecido pelo
    usuário seja uma vogal (minúscula ou maiúscula), e retorna 0,
    caso contrário.
*/
bool vogal_VOGAL(char x);
