/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 25/04/2019: Q01
    Crie um algoritmo que armazene cinco nomes em um vetor, e depois possa ser digitado um nome e,
    se for encontrado, imprimir a posição desse nome no vetor; caso contrário, imprimir uma mensagem.
*/

