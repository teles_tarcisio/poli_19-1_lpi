/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 08/04/2019: Q02
    Entrar com nome, idade e sexo de 20 pessoas.
    Imprimir o nome se a pessoa for do sexo masculino e tiver mais de 21 anos.
*/

#include <stdio.h>

//cada nome tera no maximo 30 caracteres
char nomes[20][30];
int idades[20];
char M_F[20];

void main()
{
    /*
        o laço a seguir armazena nome/idade/sexo de cada pessoa numa posição para
        cada vetor respectivo de cada dado.
        Os dados nos vetores possuem o mesmo índice para a mesma pessoa!
    */
    for (int i = 0; i <= 19; i++)       
    {
        printf("\nEntre com o nome da %da pessoa: ", (i+1));
        //scanf("%s", &nomes[i]);
        gets(nomes[i]);
        printf("\nEntre com a idade da %da pessoa: ", (i+1));
        scanf("%d", &idades[i]);
        fflush(stdin);

        printf("\nEntre com o sexo %da pessoa (M/F): ", (i+1));
        M_F[i] = getchar();
        //scanf("%c", M_F[i]);
        fflush(stdin);
        
        switch (M_F[i])
        {
            case 'M':
            case 'm':
                M_F[i] = 'M';
                fflush(stdin);
                printf("\nmasc %c", M_F[i]);
                break;

            case 'F':
            case 'f':
                M_F[i] = 'F';
                fflush(stdin);
                printf("\nfem %c", M_F[i]);
                break;
        
            case 'Q':
            case 'q':
                fflush(stdin);
                printf("\n\n tecla >> Q << pressionada, o programa sera encerrado."); //meu break do programa
                printf("\n\nPressione qualquer tecla para encerrar o programa. ");
                getchar();
                return;
            default:
                printf("\n\nFavor informar um dado valido!");
                break;
        }

    }
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */
    // testes da aquisição:
    /*
    for (int i = 0; i <= 19; i++)
    {
        printf("\n\n Nome %da pessoa: %s", (i+1), nomes[i]);
        printf("\n Idade %da pessoa: %d ", (i+1), idades[i]);
        printf("\n Sexo %da pessoa: %c", (i+1), M_F[i]);
    }
    */
    printf("\n\nHomens com 21 anos ou mais: ");
    for (int i = 0; i <= 19; i++)
    {
        if ( (M_F[i] == 'M') && (idades[i] >= 21))
        {
            printf("\n %s , %d anos", nomes[i], idades[i]);
        }

    }
    
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}