/* ********************************************************************************
   UNIVERSIDADE DE PERNAMBUCO - UPE
   ESCOLA POLITECNICA DE PERNAMBUCO - POLI
       Engenharia da Computacao
       DISCIPLINA: Linguagem de Programacao Imperativa 2019-1
       Profa.: Larissa Falcao
       Aluno: Tarcisio F. S. Teles
       CPF: 071.541.744-47
******************************************************************************** */
/* LISTA DE EXERCICIOS DE 08/04/2019: Q01
    Entrar com 10 números e imprimir a metade de cada número;
    
*/

#include <stdio.h>

float numeros[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void main()
{
    for (int i = 0; i <=9; i++)
    {
        printf("\nEntre com o %do numero da sequencia: ", (i+1));
        scanf("%f", &numeros[i]);

    }
    fflush(stdin);
    /*  !!! apos o scanf(), o '\0' ainda permanece no stdin (pois so capturou o numero),
        isto faz com que o proximo getchar() receba automaticamente o '\0' e o programa e'
        encerrado rapidamente (nao da tempo ler a msg de erro ou encerramento).
    */

    printf("\nNumeros fornecidos: \n");
    for (int j = 0; j <=9; j++)
    {
        printf(" %.2f ", numeros[j]);
    }
    printf("\n\n");

    for (int k = 0; k <=9; k++)
    {
        numeros[k] = numeros[k] / 2;
    }
    
    printf("\nCalculando as metades: \n");
    for (int t = 0; t <=9; t++)
    {
        printf(" %.2f ", numeros[t]);
    }
    
    printf("\n\nPressione qualquer tecla para encerrar o programa. ");
    getchar();
    return;
}